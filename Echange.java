package com.poto.Entity;



import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

public class Echange {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="reference")
	private Integer reference;
    private String details;
    private Integer nbreArticle;
	
	
	@ManyToOne
	@JoinColumn(name="userAuteur",insertable = false, updatable = false)
	private User user;
	private Integer userAuteur;
	
	@ManyToOne
	@JoinColumn(name="userReceveur",insertable = false, updatable = false)
	private Integer userReceveur;
	
	@ManyToOne
	@JoinColumn(name="reference",insertable = false, updatable = false)
	private Article article;
	private Integer referenceid;
	
	@ManyToOne
	@JoinColumn(name="id",insertable = false, updatable = false)
	private Categorie categorie;
	private Integer id;
	public Integer getReference() {
		return reference;
	}
	public void setReference(Integer reference) {
		this.reference = reference;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public Integer getNbreArticle() {
		return nbreArticle;
	}
	public void setNbreArticle(Integer nbreArticle) {
		this.nbreArticle = nbreArticle;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Integer getUserAuteur() {
		return userAuteur;
	}
	public void setUserAuteur(Integer userAuteur) {
		this.userAuteur = userAuteur;
	}
	public Integer getUserReceveur() {
		return userReceveur;
	}
	public void setUserReceveur(Integer userReceveur) {
		this.userReceveur = userReceveur;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public Integer getReferenceid() {
		return referenceid;
	}
	public void setReferenceid(Integer referenceid) {
		this.referenceid = referenceid;
	}
	public Categorie getCategorie() {
		return categorie;
	}
	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
}
